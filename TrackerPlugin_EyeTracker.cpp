#include "TrackerPlugin_EyeTracker.h"

TrackerPlugin_EyeTracker::TrackerPlugin_EyeTracker(QWidget *parent) : QWidget(parent)
{
    ui = new Ui::TrackerPlugin_EyeTracker();
    trackerWorker = new TrackerWorker();
    ui->setupUi(this);

    resizeHelper = new ResizeHelper(this);

    ui->label_cornea->setStyleSheet("QLabel { color : magenta; }");
    ui->label_pupil->setStyleSheet("QLabel { color : green; }");

    pixmapItem = new QGraphicsPixmapItem();

    /*Set up rectangles*/
    plotPens.push_back(new QPen(Qt::green));
    plotPens.push_back(new QPen(Qt::green));
    plotPens.push_back(new QPen(Qt::magenta));
    plotPens.push_back(new QPen(Qt::magenta));
    plotPens.push_back(new QPen(QColor(127,127,255)));

    for(int i =0;i<plotPens.size();i++){
        plotPens.at(i)->setWidth(2);
    }

    rectPens.push_back(new QPen(Qt::green));
    rectPens.push_back(new QPen(Qt::magenta));

    for(int i =0;i<rectPens.size();i++){
        rectPens.at(i)->setWidth(2);
    }

    uiPlots.push_back(ui->qwtPlot_1);
    uiPlots.push_back(ui->qwtPlot_2);
    uiPlots.push_back(ui->qwtPlot_3);
    uiPlots.push_back(ui->qwtPlot_4);
    uiPlots.push_back(ui->qwtPlot_5);

    uiSliders.push_back(ui->horizontalSlider_1);
    uiSliders.push_back(ui->horizontalSlider_2);

    uiLabels.push_back(ui->label_threshold_1);
    uiLabels.push_back(ui->label_threshold_2);

    uiLineEdits.push_back(ui->lineEdit_PupilMinArea);
    uiLineEdits.push_back(ui->lineEdit_PupilMaxArea);
    uiLineEdits.push_back(ui->lineEdit_CorneaMinArea);
    uiLineEdits.push_back(ui->lineEdit_CorneaMaxArea);

    /*Position signalmapper*/
    signalMapperPosition = new QSignalMapper(this);

    uiPlots.at(0)->setAxisTitle(0, "Pupil x [px]");
    uiPlots.at(1)->setAxisTitle(0, "Pupil y [px]");
    uiPlots.at(2)->setAxisTitle(0, "CR x [px]");
    uiPlots.at(3)->setAxisTitle(0, "CR y [px]");
    uiPlots.at(4)->setAxisTitle(0, "Pupil Ø [px]");

    /*Prepare rects*/
    for(int i=0;i<2;i++){
        rectItems.push_back(new QGraphicsRectItem());
        rectItems.at(i)->setRect(QRect(0,0,maxRectWidth,maxRectWidth));
        rectItems.at(i)->setPen(*rectPens.at(i));
        rectItems.at(i)->setFlag(QGraphicsItem::ItemIsSelectable, true);
    }

    /*Prepare curves*/
    for(int i=0;i<plotPens.size();i++){
        curves.push_back(new QwtPlotCurve("Curve"));
        curves.at(i)->setPen(*plotPens.at(i));
        curves.at(i)->setRenderHint( QwtPlotItem::RenderAntialiased );
        rectData.push_back(new QwtPointSeriesData);
        rectSamples.push_back(new QVector<QPointF>);
        curves.at(i)->attach(uiPlots.at(i));
    }

    /*Checkboxes signalmapper*/
    signalMapperCheckBoxes = new QSignalMapper(this);
    /*Connect checkboxes and sliders*/
    connect(ui->checkBox_1, SIGNAL(clicked()), signalMapperCheckBoxes, SLOT(map()));
    connect(ui->checkBox_2, SIGNAL(clicked()), signalMapperCheckBoxes, SLOT(map()));
    connect(ui->checkBox_3, SIGNAL(clicked()), signalMapperCheckBoxes, SLOT(map()));
    connect(ui->checkBox_4, SIGNAL(clicked()), signalMapperCheckBoxes, SLOT(map()));
    connect(ui->checkBox_5, SIGNAL(clicked()), signalMapperCheckBoxes, SLOT(map()));
    signalMapperCheckBoxes->setMapping(ui->checkBox_1, 0);
    signalMapperCheckBoxes->setMapping(ui->checkBox_2, 1);
    signalMapperCheckBoxes->setMapping(ui->checkBox_3, 2);
    signalMapperCheckBoxes->setMapping(ui->checkBox_4, 3);
    signalMapperCheckBoxes->setMapping(ui->checkBox_5, 4);
    connect(signalMapperCheckBoxes, SIGNAL(mapped(int)), this, SLOT(onCheckBoxClicked(int)));

    /*Sliders signalmapper*/
    signalMapperSliders = new QSignalMapper(this);
    /*Connect checkboxes and sliders*/
    connect(ui->horizontalSlider_1, SIGNAL(valueChanged(int)), signalMapperSliders, SLOT(map()));
    connect(ui->horizontalSlider_2, SIGNAL(valueChanged(int)), signalMapperSliders, SLOT(map()));
    signalMapperSliders->setMapping(ui->horizontalSlider_1, 0);
    signalMapperSliders->setMapping(ui->horizontalSlider_2, 1);
    connect(signalMapperSliders, SIGNAL(mapped(int)), this, SLOT(onThreshSliderMoved(int)));

    /*LineEdit signalmapper*/
    /*The signalmapper is an overkill at this point but since we used them for everything else, we'll stick to them for now*/
    signalMapperLineEdit = new QSignalMapper(this);
    connect(ui->lineEdit_PupilMinArea, SIGNAL(editingFinished()), signalMapperLineEdit, SLOT(map()));
    connect(ui->lineEdit_PupilMaxArea, SIGNAL(editingFinished()), signalMapperLineEdit, SLOT(map()));
    connect(ui->lineEdit_CorneaMinArea, SIGNAL(editingFinished()), signalMapperLineEdit, SLOT(map()));
    connect(ui->lineEdit_CorneaMaxArea, SIGNAL(editingFinished()), signalMapperLineEdit, SLOT(map()));
    signalMapperLineEdit->setMapping(ui->lineEdit_PupilMinArea, 0);
    signalMapperLineEdit->setMapping(ui->lineEdit_PupilMaxArea, 1);
    signalMapperLineEdit->setMapping(ui->lineEdit_CorneaMinArea, 2);
    signalMapperLineEdit->setMapping(ui->lineEdit_CorneaMaxArea, 3);
    connect(signalMapperLineEdit, SIGNAL(mapped(int)), this, SLOT(onAreaConstraintsEditingFinished(int)));

    QIntValidator *intValidator = new QIntValidator(0, 2147483647 ,this);
    ui->lineEdit->setValidator(intValidator);

    /*Connection TrackingGui - TrackingWorker*/
    connect(this, SIGNAL(thresholdChanged(int, int)), trackerWorker, SLOT(onThresholdChanged(int, int)));
    connect(this, SIGNAL(roiChanged(int,QRect)), trackerWorker, SLOT(onRoiChanged(int,QRect)));
    connect(this, SIGNAL(areaContraintsChanged(int,int)), trackerWorker, SLOT(onAreaConstraintsChanged(int,int)));
    connect(this, SIGNAL(doTracking(bool)), trackerWorker, SLOT(onDoTracking(bool)));
    connect(ui->checkBox_showBinary, SIGNAL(clicked(bool)), trackerWorker, SLOT(onShowBinary(bool)));
    connect(ui->checkBox_brightPupil, SIGNAL(clicked(bool)), trackerWorker, SLOT(onBrightPupil(bool)));
    connect(trackerWorker, SIGNAL(trackingResult(std::vector<double>)), this, SLOT(onTrackingResult(std::vector<double>)));
    connect(resizeHelper, SIGNAL(itemChanged(QGraphicsItem*)),this, SLOT(onGraphicsItemChanged(QGraphicsItem*)));
}

TrackerPlugin_EyeTracker::~TrackerPlugin_EyeTracker()
{
    /*Clean up Memory*/
    for(QPen* pen : plotPens)
        delete pen;

    for(QPen* pen : rectPens)
        delete pen;

    for(auto rectSample : rectSamples)
        delete rectSample;

    delete pixmapItem;
    delete trackerWorker;
    delete ui;
}


QObject *TrackerPlugin_EyeTracker::worker()
{
    return trackerWorker;
}

void TrackerPlugin_EyeTracker::initializeUI(QLayout *layout, QGraphicsScene *cameraScene, QSize cameraResolution)
{
    /*Keep rectangles unchanged if the resolution didn't change*/
    if(this->cameraResolution!=cameraResolution){
        this->cameraResolution = cameraResolution;
        layout->addWidget(this);
        this->cameraScene = cameraScene;

        /*Make objects resizeable and moveable*/
        resizeHelper->boundsRect = QRectF(0,0,cameraResolution.width(),cameraResolution.height());
        this->cameraScene->addItem(resizeHelper);
        connect(this->cameraScene, SIGNAL(selectionChanged()), resizeHelper, SLOT(selectionChanged()));

        for(int i=0;i<rectItems.size();i++){
            this->cameraScene->addItem(rectItems.at(i));
        }

        /*Resize rectangles according to the correct size*/
        if(cameraResolution.width()>cameraResolution.height()){
            this->maxRectWidth = cameraResolution.height()/2;
        }else{
            this->maxRectWidth = cameraResolution.width()/2;
        }
        for(int i=0;i<rectItems.size();i++){
            rectItems.at(i)->setRect(QRect(0,0,maxRectWidth,maxRectWidth));
            emit(thresholdChanged(i, uiSliders.at(i)->value()));
            emit(roiChanged(i, rectItems.at(i)->rect().toRect()));
        }
    }
}

void TrackerPlugin_EyeTracker::onTrackingResult(std::vector<double> result)
{
    for(int i = 0; i< rectSamples.size(); i++){
        if(frameCount>wndLength)/*window is full - stop */
            rectSamples.at(i)->pop_front();
        rectSamples.at(i)->push_back(QPointF(t, result.at(i)));
        rectData.at(i)->setSamples(*rectSamples.at(i));
        curves.at(i)->setData(rectData.at(i));
    }
    ui->qwtPlot_1->replot();
    ui->qwtPlot_2->replot();
    ui->qwtPlot_3->replot();
    ui->qwtPlot_4->replot();
    ui->qwtPlot_5->replot();

    t++;
    frameCount++;
}

void TrackerPlugin_EyeTracker::onCheckBoxClicked(int index)
{
    bool isVisible = !uiPlots.at(index)->isVisible();

    switch ( index ){
    case 0:
        rectItems.at(index)->setVisible(isVisible);
        uiSliders.at(index)->setVisible(isVisible);
        break;
    case 2:
        rectItems.at(1)->setVisible(isVisible);
        uiSliders.at(1)->setVisible(isVisible);
        break;
    }

    uiPlots.at(index)->setVisible(isVisible);
}

void TrackerPlugin_EyeTracker::onThreshSliderMoved(int index)
{
    float value = (uiSliders.at(index)->value());
    uiLabels.at(index)->setText(QString("Threshold: ").append(QString::number(int(value))));
    emit(thresholdChanged(index, value));
}

void TrackerPlugin_EyeTracker::onGraphicsItemChanged(QGraphicsItem *item)
{
    int rectIdx = -1;
    /*Determine which rectangle was changed*/
    for(int i = 0; i<rectItems.size(); i++){
        if(dynamic_cast<QGraphicsRectItem*>(item) == rectItems.at(i)){
            rectIdx = i;
        }
    }
    /*If the resized object is a ROI rectangle created by this plugin*/
    /*Send message*/
    if(rectIdx >= 0){
        emit(roiChanged(rectIdx, rectItems.at(rectIdx)->rect().toRect()));
    }
}

void TrackerPlugin_EyeTracker::on_lineEdit_editingFinished()
{
    wndLength = ui->lineEdit->text().toDouble();
    for(int i=0;i<rectSamples.size();i++){
        rectSamples.at(i)->clear();
        frameCount = 0;
    }
}

void TrackerPlugin_EyeTracker::onAreaConstraintsEditingFinished(int index)
{
    emit(areaContraintsChanged(index, uiLineEdits.at(index)->text().toInt()));
}

void TrackerPlugin_EyeTracker::on_checkBox_doTracking_clicked(bool checked)
{
    emit(doTracking(checked));
}
