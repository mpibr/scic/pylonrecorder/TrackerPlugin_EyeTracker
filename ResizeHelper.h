#ifndef ResizeHelper_H
#define ResizeHelper_H

#include <QObject>
#include <QGraphicsObject>
#include <QPen>
#include <QPainter>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <ResizeTraits.h>
#include <QDebug>

class ResizeHelper : public QGraphicsObject
{
    Q_OBJECT

public:
    QRectF boundsRect; /*Defines the bounding area in which object can be moved/resized*/
    ResizeHelper(QObject *parent = 0);

private:
    ResizeTraits traits;
    QRectF rubberRect;
    QPen rubberPen;
    QFlags<Qt::Edge> rubberEdges;
    QPointF clickPos; /*Last position used to detect movement*/
    QRectF boundingRect() const Q_DECL_OVERRIDE;
    void newGeometry(); /*Propagate geometry change to bounding rectangle*/
    void paint(QPainter * p, const QStyleOptionGraphicsItem *, QWidget *);
    void mousePressEvent(QGraphicsSceneMouseEvent * ev) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QGraphicsSceneMouseEvent * ev) Q_DECL_OVERRIDE;

signals:
    void itemChanged(QGraphicsItem* item);
public slots:
    void selectionChanged();
};

#endif // ResizeHelper_H
