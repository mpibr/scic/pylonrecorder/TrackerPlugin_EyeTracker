# Description
Plugin to perform eye tracking with [PylonRecorder](https://software.scic.corp.brain.mpg.de/projects/PylonRecorder/). 
Detects the location of the pupil and a corneal reflection in a video. Fits an ellipse around the pupil to determine
its diameter.

# Algorithm
Two threshold are being applied to determine the pupil as the darkest area
in the image and the corneal reflection as the brightest area in the image.
A minimum and maximum Size can be specified for these areas. In case of
the pupil an ellipse is fitted around the resulting binary image and the
mean of both perpendicular axes is used to calculate the diameter. The center
of the ellipse describes the center of the pupil. In case of the corneal
reflection the center of mass of the binary image is used to determine
the corneas location.

# Usage
1. Move and resize the rectangles to set the rois around pupil and the corneal reflection.
2. Enable Tracking preview in pylonRecorder and enable the "binary" checkbox to
show the thresholded image. 
3. Use the sliders to adjust the threshold until
the pupil and corneal reflection are well separated from the background.
4. Disable the "binary" threshold once you are happy with the tracking result.

## Plugins
To use this plugin for PylonRecorder you need to download and place 
the plugin's shared library (on windows a .dll file) into the PylonRecorder 
program folder. PylonRecorder will then automatically detect and load the plugin.

# Bugs
Please report all bugs [here](mailto:sciclist@brain.mpg.de)
