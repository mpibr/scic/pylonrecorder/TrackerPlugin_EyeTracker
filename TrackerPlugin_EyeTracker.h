#ifndef EYETRACKER_H
#define EYETRACKER_H

#include "TrackerInterface.h"
#include "TrackerWorker.h"
#include "ui_TrackerPlugin_EyeTracker.h"
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_series_data.h>
#include <QGraphicsScene>
#include <QGraphicsPixmapItem>
#include <QDebug>
#include <QSignalMapper>
#include <qwt_slider.h>
#include <QPoint>
#include <QSize>
#include <QObject>
#include <QDebug>
#include "ResizeHelper.h"

namespace Ui {
class TrackerPlugin_EyeTracker;
}

class TrackerPlugin_EyeTracker : public QWidget, public TrackerInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.PylonRecorder.TrackerInterface")
    Q_INTERFACES(TrackerInterface)

public:
    explicit TrackerPlugin_EyeTracker(QWidget *parent = 0);
    ~TrackerPlugin_EyeTracker();
    QObject *worker();
    void initializeUI(QLayout* guiTargetLayout, QGraphicsScene* cameraScene, QSize cameraResolution);
    TrackerWorker* trackerWorker;
    QSize cameraResolution = QSize(0,0);

private:
    Ui::TrackerPlugin_EyeTracker *ui;
    QGraphicsScene* cameraScene;
    QGraphicsPixmapItem* pixmapItem;
    QVector<QGraphicsRectItem*> rectItems;
    QVector<QPen*> plotPens;
    QVector<QPen*> rectPens;

    QVector<QVector<QPointF>*> rectSamples;
    QVector<QwtPointSeriesData*> rectData;
    QVector<QwtPlotCurve*> curves;

    /*ui pointers for use with signalmapper*/
    QVector<QwtPlot*> uiPlots;
    QVector<QSlider*> uiSliders;
    QVector<QLabel*> uiLabels;
    QVector<QCheckBox*> uiCheckboxes;
    QVector<QLineEdit*> uiLineEdits;

    int maxRectWidth = 200;
    const int bitDepth = 8;

    int t = 0;
    int wndLength = 100;
    int frameCount = 0;

    QSignalMapper* signalMapperPosition;
    QSignalMapper* signalMapperCheckBoxes;
    QSignalMapper* signalMapperSliders;
    QSignalMapper* signalMapperLineEdit;

    ResizeHelper* resizeHelper; //Manages resizing of items in scene

private slots:
    void onTrackingResult(std::vector<double>);
    void onCheckBoxClicked(int);
    void onThreshSliderMoved(int);
    void onGraphicsItemChanged(QGraphicsItem* item);
    void on_lineEdit_editingFinished();
    void onAreaConstraintsEditingFinished(int);

    void on_checkBox_doTracking_clicked(bool checked);

signals:
    void roiChanged(int index, QRect roi);
    void thresholdChanged(int index, int value);
    void areaContraintsChanged(int index, int value);
    void doTracking(bool isTrue);

};

#endif // EYETRACKER_H
