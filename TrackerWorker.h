#ifndef TRACKERWORKER_H
#define TRACKERWORKER_H

#include <QDebug>
#include <QtGlobal>
#include <QVector>
#include <QRect>
#include <QTimer>
#include "TrackerWorkerInterface.h"
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/features2d.hpp>
using namespace cv;

class TrackerWorker : public QObject, public TrackerWorkerInterface
{
    Q_OBJECT

public:
    TrackerWorker(QObject* parent = 0);

private:
    QVector<QRect> rois;
    QVector<int> thresholds;
    QVector<int> areaConstraints; /*Pupil minArea, maxArea, Cornea minArea, maxArea*/
    bool showBinary = false;
    bool isBrightPupil = false;
    bool isRoiStable = false;
    const int MaxNumberContours = 50;


public slots:
    void onFrameGrabbed(Mat);
    void onThresholdChanged(int index, int value);
    void onAreaConstraintsChanged(int minSize, int maxSize);
    void onRoiChanged(int index, QRect roi);
    void onShowBinary(bool showBinary);
    void onBrightPupil(bool isInverted);
    void onDoTracking(bool doTracking);

signals:
    void trackingPreview(Mat);
    void trackingRois(QVector<Mat>);
    void trackingResult(std::vector<double> data); /*The results are stored in a regular vector for easier communication with non-Qt C++ programs (e.g. through socket)*/
    void userOutput(bool value);

};

#endif // TRACKERWORKER_H
