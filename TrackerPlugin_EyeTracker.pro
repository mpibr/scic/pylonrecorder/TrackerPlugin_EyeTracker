#-------------------------------------------------
#
# Project created by QtCreator 2016-10-24T11:57:07
#
#-------------------------------------------------

TEMPLATE      = lib
CONFIG       += plugin
QT           += widgets
TARGET        = $$qtLibraryTarget(TrackerPlugin_EyeTracker)
DESTDIR       = ../../../../src/build/plugins
CONFIG += c++11


SOURCES += \
    TrackerWorker.cpp \
    TrackerPlugin_EyeTracker.cpp \
    ResizeHelper.cpp \
    ResizeTraits.cpp

HEADERS += \
    TrackerWorker.h \
    TrackerPlugin_EyeTracker.h \
    ResizeHelper.h \
    ResizeTraits.h

INCLUDEPATH += ../../../src/

win32 {
    INCLUDEPATH += "D:\\opencv\\build_vc12\\install\\include"

    CONFIG(debug,debug|release) {
        LIBS += -L"D:\\opencv\\build_vc12\\install\\x64\\vc12\\lib" \
            -lopencv_core310 \
            -lopencv_highgui310 \
            -lopencv_imgproc310 \
            -lopencv_features2d310 \
            -lopencv_videoio310 \
            -lopencv_video310 \
            -lopencv_videostab310 \
    }

    CONFIG(release,debug|release) {
        DEFINES += QT_NO_WARNING_OUTPUT QT_NO_DEBUG_OUTPUT
        LIBS += -L"D:\\opencv\\build_vc12\\install\\x64\\vc12\\lib" \
            -lopencv_core310 \
            -lopencv_highgui310 \
            -lopencv_imgproc310 \
            -lopencv_features2d310 \
            -lopencv_videoio310 \
            -lopencv_video310 \
            -lopencv_videostab310 \
    }

    INCLUDEPATH += "C:/Qwt-6.1.3/include"
    LIBS += -L"C:/Qwt-6.1.3/lib" \
        -lqwt
}

unix{
    INCLUDEPATH += "/usr/local/include/opencv2"

    LIBS += -L"/usr/local/lib" \
            -lopencv_core \
            -lopencv_highgui \
            -lopencv_imgproc \
            -lopencv_features2d \
            -lopencv_videoio \
            -lopencv_video \
            -lopencv_videostab

    CONFIG += qwt #only works on 5.21
    INCLUDEPATH += /usr/include/qwt
    LIBS += -L/usr/lib/x86_64-linux-gnu
    LIBS += -lqwt
}

FORMS += \
    TrackerPlugin_EyeTracker.ui
