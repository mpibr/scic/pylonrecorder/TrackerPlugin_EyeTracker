#include "ResizeHelper.h"

/// The set of edges intersecting a rectangle of given pen width
QFlags<Qt::Edge> edgesAt(const QPointF & p, const QRectF & r, qreal w) {
    QFlags<Qt::Edge> edges;
    auto hw = w / 2.0;
    if (QRectF(r.x()-hw, r.y()-hw, w, r.height()+w).contains(p)) edges |= Qt::LeftEdge;
    if (QRectF(r.x()+r.width()-hw, r.y()-hw, w, r.height()+w).contains(p)) edges |= Qt::RightEdge;
    if (QRectF(r.x()-hw, r.y()-hw, r.width()+w, w).contains(p)) edges |= Qt::TopEdge;
    if (QRectF(r.x()-hw, r.y()+r.height()-hw, r.width()+w, w).contains(p)) edges |= Qt::BottomEdge;
    return edges;
}

ResizeHelper::ResizeHelper(QObject *parent) {
    setAcceptedMouseButtons(Qt::LeftButton);
}

QRectF ResizeHelper::boundingRect() const {
    auto hWidth = rubberPen.widthF()/2.0;
    return rubberRect.adjusted(-hWidth, -hWidth, hWidth, hWidth);
}

void ResizeHelper::selectionChanged() {
    if (!scene()) { setVisible(false); return; }
    auto sel = scene()->selectedItems();
    if (sel.isEmpty() || sel.size() > 1) { setVisible(false); return; }
    auto item = sel.at(0);
    if (! traits.isGraphicsItemResizeable(item)) { setVisible(false); return; }
    rubberPen = traits.penFor(item);
    setParentItem(item);
    newGeometry();
    setVisible(true);
}

void ResizeHelper::paint(QPainter * p, const QStyleOptionGraphicsItem *, QWidget *) {
    p->setPen(rubberPen);
    p->drawRect(rubberRect);
}

void ResizeHelper::mousePressEvent(QGraphicsSceneMouseEvent * ev) {
    rubberEdges = edgesAt(ev->pos(), rubberRect, rubberPen.widthF());
    if (!rubberEdges){ /*Moving: Store click location*/
        //ev->ignore(); /*Ignore if moved*/
        clickPos = ev->pos();
    }
    ev->accept();
}

void ResizeHelper::mouseMoveEvent(QGraphicsSceneMouseEvent * ev) {

    auto pos = mapToItem(parentItem(), ev->pos());
    auto rect = traits.rectFor(parentItem());
    if (!!rubberEdges) {
        /*Edge was clicked -> Resize*/
        if (rubberEdges & Qt::LeftEdge) rect.setLeft(pos.x());
        if (rubberEdges & Qt::TopEdge) rect.setTop(pos.y());
        if (rubberEdges & Qt::RightEdge) rect.setRight(pos.x());
        if (rubberEdges & Qt::BottomEdge) rect.setBottom(pos.y());
    }else{ /*No Edge clicked -> Moving*/
        QPointF d = clickPos - pos;
        rect.setLeft(rect.left()-d.x());
        rect.setTop(rect.top()-d.y());
        rect.setRight(rect.right()-d.x());
        rect.setBottom(rect.bottom()-d.y());
        clickPos = pos;
    }

    /*Make sure that rectangle is within predefined boundaries (if given)*/
    if ((rect.width()>0 & rect.height()>0) & ((!boundsRect.isEmpty() & (boundsRect.contains(rect))) | boundsRect.isEmpty())){
        traits.setRectOn(parentItem(), rect);
        newGeometry();
        emit(itemChanged(parentItem()));
    }
}

void ResizeHelper::newGeometry() {
    /*Propagate geometry change to bounding rectangle*/
    prepareGeometryChange();
    auto parentRect = traits.rectFor(parentItem());
    rubberRect.setTopLeft(mapFromParent(parentRect.topLeft()));
    rubberRect.setBottomRight(mapFromParent(parentRect.bottomRight()));
}
